var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

const donations = require("./routes/donations");
const dishes = require("./routes/dishes");;
const comments = require("./routes/dishedComment");

const user = require('./routes/user')

const googleCallback = require('./routes/googleCallback')

var app = express();


app.all("*",function(req,res,next){
  //设置允许跨域的域名，*代表允许任意域名跨域
  res.header("Access-Control-Allow-Origin","*");
  //允许的header类型
  res.header("Access-Control-Allow-Headers","content-type");
  //跨域允许的请求方式
  res.header("Access-Control-Allow-Methods","DELETE,PUT,POST,GET,OPTIONS");
  if (req.method.toLowerCase() == 'options')
    res.send(200);  //让options尝试请求快速结束
  else
    next();
});


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// Our Custom Donation Web App Routes
app.get('/donation', donations.addDonation);
app.get('/donations', donations.findAll);
app.get('/donations/votes', donations.findTotalVotes);
app.get('/donations/:id', donations.findOne);
app.post('/donations',donations.addDonation);
app.put('/donations/:id/vote', donations.incrementUpvotes);
app.delete('/donations/:id', donations.deleteDonation);


app.get('/dishes/', dishes.findAll);
app.get('/dishes/:id', dishes.findOne);
app.post('/dished/', dishes.addDished);
app.delete('/dished/:id',dishes.deleteDished);
app.put('/dished/:id/vote',dishes.incrementUpvotes);
app.put('/dished/:id/dvote',dishes.reductionUpvotes);

app.get('/comments/', comments.findAll);
app.get('/comments/:id', comments.findOne);
app.post('/comments/', comments.addDished);
app.delete('/comment/:id',comments.deleteDished);
app.put('/comment/:id/vote',comments.incrementUpvotes);
app.put('/comment/:id/dvote',comments.reductionUpvotes);


app.post('/user/',user.addUser);
app.get('/user/:username/:password',user.findOne);


app.get('/google/callback',googleCallback.callback);
app.get('/google/url',googleCallback.getAuthUrl);



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
