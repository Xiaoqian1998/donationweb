let Dished = require('../models/dished');
let express = require('express');
let router = express.Router();
let mongoose = require('mongoose');
let uriUtil = require('mongodb-uri');

const mongodbUri = "mongodb://lxq:qmlpTrVYVmTqZ58s@cluster0-shard-00-00-szspm.mongodb.net:27017,cluster0-shard-00-01-szspm.mongodb.net:27017,cluster0-shard-00-02-szspm.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority";
mongoose.connect(mongodbUri);

//mongoose.connect('mongodb://localhost:27017/Dishedsdb');

let db = mongoose.connection;

db.on('error', function (err) {
    console.log('Unable to Connect to [ ' + db.name + ' ]', err);
});

db.once('open', function () {
    console.log('Successfully Connected to [ ' + db.name + ' ] on mlab.com');
});

router.findAll = (req, res) => {
    // Return a JSON representation of our list
    res.setHeader('Content-Type', 'application/json');

    Dished.find(function(err, Disheds) {
        if (err)
            res.send(err);

        res.send(JSON.stringify(Disheds,null,5));
    });
}

router.findOne = (req, res) => {

    res.setHeader('Content-Type', 'application/json');

    Dished.find({ "_id" : req.params.id },function(err, Dished) {
        if (err)
            res.json({ message: 'Dished NOT Found!', errmsg : err } );
        else
            res.send(JSON.stringify(Dished,null,5));
    });
}

router.updateByCode = (req, res) => {
    res.setHeader('Content-Type', 'application/json');

    Dished.update({ "code" : req.body.code },function(err, Dished) {
        if (err)
            res.json({ message: 'Dished NOT Found!', errmsg : err } );
        else
            res.send(JSON.stringify(Dished,null,5));
    });
}

function getTotalVotes(array) {
    let totalVotes = 0;
    array.forEach(function(obj) { totalVotes += obj.upvotes; });
    return totalVotes;
}

router.addDished = (req, res) => {

    res.setHeader('Content-Type', 'application/json');

    var dished = new Dished();

    dished.name = req.body.name;
    dished.code = req.body.code;
    dished.price = req.body.price;


    dished.save(function(err) {
        if (err)
            res.json({ message: 'Dished NOT Added!', errmsg : err } );
        else
            res.json({ message: 'Dished Successfully Added!', data: dished });
    });
}

// router.incrementUpvotes = (req, res) => {
//     // Find the relevant Dished based on params id passed in
//     // Add 1 to upvotes property of the selected Dished based on its id
//     var Dished = getByValue(Disheds,req.params.id);
//
//     if (Dished != null) {
//         Dished.upvotes += 1;
//         res.json({status : 200, message : 'UpVote Successful' , Dished : Dished });
//     }
//     else
//         res.send('Dished NOT Found - UpVote NOT Successful!!');
//
// }

router.incrementUpvotes = (req, res) => {

    Dished.findById(req.params.id, function(err,Dished) {
        if (err)
            res.json({ message: 'Dished NOT Found!', errmsg : err } );
        else {
            Dished.upvotes += 1;
            Dished.save(function (err) {
                if (err)
                    res.json({ message: 'Dished NOT UpVoted!', errmsg : err } );
                else
                    res.json({ message: 'Dished Successfully Upvoted!', data: Dished });
            });
        }
    });
}

router.reductionUpvotes = (req, res) => {
    Dished.findById(req.params.id, function(err,Dished) {
        if (err)
            res.json({ message: 'Dished NOT Found!', errmsg : err } );
        else {
            if(Dished.upvotes < 1) return;
            Dished.upvotes -= 1;
            Dished.save(function (err) {
                if (err)
                    res.json({ message: 'Dished NOT Downvoted!', errmsg : err } );
                else
                    res.json({ message: 'Dished Successfully Downvoted!', data: Dished });
            });
        }
    });
}

router.deleteDished = (req, res) => {

    Dished.findByIdAndRemove(req.params.id, function(err) {
        if (err)
            res.json({ message: 'Dished NOT DELETED!', errmsg : err } );
        else
            res.json({ message: 'Dished Successfully Deleted!'});
    });
}

router.findTotalVotes = (req, res) => {

    Dished.find(function(err, Disheds) {
        if (err)
            res.send(err);
        else
            res.json({ totalvotes : getTotalVotes(Disheds) });
    });
}

module.exports = router;