let DishedComment = require('../models/dishedComment');
let express = require('express');
let router = express.Router();
let mongoose = require('mongoose');
let uriUtil = require('mongodb-uri');

const mongodbUri = "mongodb://lxq:qmlpTrVYVmTqZ58s@cluster0-shard-00-00-szspm.mongodb.net:27017,cluster0-shard-00-01-szspm.mongodb.net:27017,cluster0-shard-00-02-szspm.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority";

mongoose.connect(mongodbUri);

//mongoose.connect('mongodb://localhost:27017/commentsdb');

let db = mongoose.connection;

db.on('error', function (err) {
    console.log('Unable to Connect to [ ' + db.name + ' ]', err);
});

db.once('open', function () {
    console.log('Successfully Connected to [ ' + db.name + ' ] on mlab.com');
});

router.findAll = (req, res) => {
    // Return a JSON representation of our list
    res.setHeader('Content-Type', 'application/json');

    DishedComment.find(function(err, comments) {
        if (err)
            res.send(err);

        res.send(JSON.stringify(comments,null,5));
    });
}

router.findOne = (req, res) => {

    res.setHeader('Content-Type', 'application/json');

    DishedComment.find({ "_id" : req.params.id },function(err, DishedComment) {
        if (err)
            res.json({ message: 'DishedComment NOT Found!', errmsg : err } );
        else
            res.send(JSON.stringify(DishedComment,null,5));
    });
}

router.updateByCode = (req, res) => {
    res.setHeader('Content-Type', 'application/json');

    DishedComment.update({ "code" : req.body.code },function(err, DishedComment) {
        if (err)
            res.json({ message: 'DishedComment NOT Found!', errmsg : err } );
        else
            res.send(JSON.stringify(DishedComment,null,5));
    });
}

function getTotalVotes(array) {
    let totalVotes = 0;
    array.forEach(function(obj) { totalVotes += obj.upvotes; });
    return totalVotes;
}

router.addDished = (req, res) => {

    res.setHeader('Content-Type', 'application/json');

    var dished = new DishedComment();

    dished.title = req.body.title;
    dished.code = req.body.code;
    dished.message = req.body.message;

    if(req.body.id){
        DishedComment.findById(req.body.id, function(err,dishedComment) {
            if (err)
                res.json({ message: 'DishedComment NOT Found!', errmsg : err } );
            else {
                dishedComment.title = req.body.title;
                dishedComment.code = req.body.code;
                dishedComment.message = req.body.message;
                dishedComment.save(function (err) {
                    if (err)
                        res.json({ message: 'Add DishedComment Failed!', errmsg : err } );
                    else
                        res.json({ message: 'Add DishedComment Successfully!', data: dishedComment });
                });
            }
        });
        return
    }
    dished.save(function(err) {
        if (err)
            res.json({ message: 'DishedComment NOT Added!', errmsg : err } );
        else
            res.json({ message: 'DishedComment Successfully Added!', data: dished });
    });
}

// router.incrementUpvotes = (req, res) => {
//     // Find the relevant DishedComment based on params id passed in
//     // Add 1 to upvotes property of the selected DishedComment based on its id
//     var DishedComment = getByValue(comments,req.params.id);
//
//     if (DishedComment != null) {
//         DishedComment.upvotes += 1;
//         res.json({status : 200, message : 'UpVote Successful' , DishedComment : DishedComment });
//     }
//     else
//         res.send('DishedComment NOT Found - UpVote NOT Successful!!');
//
// }

router.incrementUpvotes = (req, res) => {

    DishedComment.findById(req.params.id, function(err,dishedComment) {
        if (err)
            res.json({ message: 'DishedComment NOT Found!', errmsg : err } );
        else {
            dishedComment.upvotes += 1;
            dishedComment.save(function (err) {
                if (err)
                    res.json({ message: 'DishedComment NOT UpVoted!', errmsg : err } );
                else
                    res.json({ message: 'DishedComment Successfully Upvoted!', data: dishedComment });
            });
        }
    });
}

router.reductionUpvotes = (req, res) => {
    DishedComment.findById(req.params.id, function(err,DishedComment) {
        if (err)
            res.json({ message: 'DishedComment NOT Found!', errmsg : err } );
        else {
            if(DishedComment.upvotes < 1) return;
            DishedComment.upvotes -= 1;
            DishedComment.save(function (err) {
                if (err)
                    res.json({ message: 'DishedComment NOT UpVoted!', errmsg : err } );
                else
                    res.json({ message: 'DishedComment Successfully Upvoted!', data: DishedComment });
            });
        }
    });
}

router.deleteDished = (req, res) => {

    DishedComment.findByIdAndRemove(req.params.id, function(err) {
        if (err)
            res.json({ message: 'DishedComment NOT DELETED!', errmsg : err } );
        else
            res.json({ message: 'DishedComment Successfully Deleted!'});
    });
}

router.findTotalVotes = (req, res) => {

    DishedComment.find(function(err, comments) {
        if (err)
            res.send(err);
        else
            res.json({ totalvotes : getTotalVotes(comments) });
    });
}

module.exports = router;