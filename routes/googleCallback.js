var express = require('express');
var router = express.Router();
const {OAuth2Client} = require('google-auth-library');

const keys = require('./oauth2.keys.json');

const oAuth2Client = new OAuth2Client(
    keys.web.client_id,
    keys.web.client_secret,
    keys.web.redirect_uris[0]
);


router.callback = (requset, response) => {
    var code = requset.query.code
    const url = 'https://people.googleapis.com/v1/people/me?personFields=names';
    const res = oAuth2Client.request({url});
    console.log(res.data);

    // After acquiring an access_token, you may want to check on the audience, expiration,
    // or original scopes requested.  You can do that with the `getTokenInfo` method.
    const tokenInfo =  oAuth2Client.getTokenInfo(
        oAuth2Client.credentials.access_token
    );
    console.log(tokenInfo);
}

router.getAuthUrl = () => {
    const authorizeUrl = oAuth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: 'https://www.googleapis.com/auth/userinfo.profile',
        prompt: 'consent'
    });
    return authorizeUrl
}

module.exports = router;
