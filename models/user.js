let mongoose = require('mongoose');

let UserSchema = new mongoose.Schema({
        username: String,
        password: String
    },
    { collection: 'userdb' });

module.exports = mongoose.model('User', UserSchema);