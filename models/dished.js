let mongoose = require('mongoose');

let DishedSchema = new mongoose.Schema({
        name: String,
        code: String,
        price: Number,
        upvotes: {type: Number, default: 0}
    },
    { collection: 'disheddb' });

module.exports = mongoose.model('Dished', DishedSchema);